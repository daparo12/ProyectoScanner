import nodo

class Arbol:

    def __init__(self, nodo):
        self.nodo= nodo
        self.hijos = []

    def agregarNodo(arbol, nodo, nodoPadre):
        subarbol = Arbol.buscarSubarbol(arbol, nodoPadre)
        subarbol.hijos.append(Arbol(nodo))
        if len(nodo.lista_hijos)>=0:
            for hijo in nodo.lista_hijos:
                Arbol.agregarNodo(arbol,hijo,nodo)


    def buscarSubarbol(arbol, nodo):
        if arbol.nodo == nodo:
            return arbol
        for subarbol in arbol.hijos:
            arbolBuscado = Arbol.buscarSubarbol(subarbol, nodo)
            if (arbolBuscado != None):
                return arbolBuscado
        return None

    def preOrden(self,nodo):
        if(nodo==null):
            return
        else:
            print(nodo.tipoToken)
            for i in nodo.lista_hijos:
                preOrden(i)

