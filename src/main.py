# -*- coding: utf-8 -*-
import explorador
import arbol
import nodo
import parser
import sys



# Esta funcion es la encargada de ejecutar el scanner
def main():
    listaTokens = explorador.Explorador(sys.argv[1]).leerArchivo()
    parser.Parser(listaTokens).parserTokens()


    #def ejecutarProfundidadPrimero(arbol, funcion):
     #   funcion(arbol.nodo)
      #  for hijo in arbol.hijos:
       #     ejecutarProfundidadPrimero(hijo, funcion)



    #def printNodo(nodo):
     #   print(nodo.textoToken)
    #ejecutarProfundidadPrimero(arbolASA, printNodo)

main()
