# -*- coding: utf-8 -*-
import nodo
import arbol


class Parser():
    def __init__(self, listaTokens):
        self.listaTokens = listaTokens
        self.elementoActual = 0
        self.arbolASA = None
        self.lista_asignaciones = []
        self.lista_parametro = []
        self.lista_parametros = []
        self.lista_expresiones = []
        self.tamListaTokens = len(listaTokens) - 1

    def parserTokens(self):
        self.lenguaje()

    def lenguaje(self):
        while self.elementoActual < self.tamListaTokens:
            funcion = self.validarFuncion()
            nodoLenguaje = nodo.Nodo("lenguaje", [funcion], 0)
            self.arbolASA = arbol.Arbol(nodoLenguaje)
            arbol.Arbol.agregarNodo(self.arbolASA, funcion, nodoLenguaje)
            self.ejecutarProfundidadPrimero(self.arbolASA)
        # self.validarFuncion()
        # self.validarInicio("lenguaje")

    def validarAsignacion(self):
        if self.listaTokens[self.elementoActual][1] == "sal" or self.listaTokens[self.elementoActual][1] == "lizano":
            self.elementoActual += 1
            variable = self.validarVariable()
            if self.listaTokens[self.elementoActual][1] == "=>":
                self.elementoActual += 1
                dato = self.validarDato()
                if dato is not None:
                    aritmetica = self.validarAritmetica()
                    if aritmetica is not None:
                        dato2 = self.validarDato()
                        if dato2 is not None:
                            nodotemp = nodo.Nodo("Asignacion", [variable, dato, aritmetica, dato2], self.listaTokens[self.elementoActual][2])
                            return nodotemp
                    else:
                        nodotemp = nodo.Nodo("Asignacion", [variable, dato], self.listaTokens[self.elementoActual][2])
                        return nodotemp
                llamadaFuncion = self.validarLlamadaFuncion()
                if llamadaFuncion is not None:
                    nodotemp = nodo.Nodo("Asignacion", [variable, llamadaFuncion], self.listaTokens[self.elementoActual][2])
                    return nodotemp
                else:
                    print("Error Sintaxis, despues de => se esperaba un dato o llamadaFuncion")
            else:
                print("Error Sintaxis, se esperaba =>")
        else:
            print("Error Sintaxis, se esperaba palabra reservada sal o lizano")

    def validarFuncion(self):
        if self.listaTokens[self.elementoActual][1] == "platillo":
            self.elementoActual += 1
            nombre = self.validarNombre()
            if nombre is not None:
                if self.listaTokens[self.elementoActual][1] == "(":
                    self.elementoActual += 1
                    parametros = self.validarParametros()
                    if parametros is not None:
                        if self.listaTokens[self.elementoActual][1] == ")":
                            self.elementoActual += 1
                            if self.listaTokens[self.elementoActual][1] == "[":
                                self.elementoActual += 1
                                instrucciones = self.validarInstrucciones()
                                if instrucciones is not None:
                                    final = self.validarFinal()
                                    if final is not None:
                                        if self.listaTokens[self.elementoActual][1] == "]":
                                            nodoTemp = nodo.Nodo("Funcion", [nombre, parametros, instrucciones, final], self.listaTokens[self.elementoActual][2])
                                            return nodoTemp
                                        else:
                                            print("Error Sintaxis, se esperaba un ]")
                                    else:
                                        print("Error Sintaxis, se esperaba un final")
                                else:
                                    print("Error Sintaxis, se esperaba instruccciones")
                            else:
                                print("Error Sintaxis, se esperaba [")
                        else:
                            print("Error Sintaxis, se esperaba un )")
                    else:
                        if self.listaTokens[self.elementoActual][1] == ")":
                            self.elementoActual += 1
                            if self.listaTokens[self.elementoActual][1] == "[":
                                self.elementoActual += 1
                                instrucciones = self.validarInstrucciones()
                                if instrucciones is not None:
                                    final = self.validarFinal()
                                    if final is not None:
                                        if self.listaTokens[self.elementoActual][1] == "]":
                                            self.elementoActual += 1
                                            nodoTemp = nodo.Nodo("Funcion", [nombre, instrucciones, final], self.listaTokens[self.elementoActual][2])
                                            return nodoTemp
                                        else:
                                            print("Error Sintaxis, se esperaba un ]")
                                    else:
                                        print("Error Sintaxis, se esperaba final")
                                else:
                                    print("Error Sintaxis, se esperaba instrucciones")
                            else:
                                print("Error Sintaxis, se esperaba un [")
                        else:
                            print("Error Sintaxis, se esperaba un )")
                else:
                    print("Error Sintaxis, se esperaba un (")
            else:
                print("Error Sintaxis, se esperaba un nombre")
        else:
            print("Error Sintaxis, se esperaba una platillo")

    def validarInicio(self):
        listaNodos = []
        instruccion = self.validarInstrucciones()
        funcion = self.validarLlamadaFuncion()
        if instruccion is not None:
            listaNodos.append(instruccion)
        if funcion is not None:
            listaNodos.append(funcion)
        nodoTemp = nodo.Nodo('Inicio', listaNodos, self.listaTokens[self.elementoActual][2])
        return nodoTemp

    def validarVariable(self):
        nombre = self.validarNombre()
        if nombre is not None:
            nodoTemp = nodo.Nodo("Variable", [nombre], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        else:
            return None

    def validarDato(self):
        variable = self.validarVariable()
        if variable is not None:
            nodoTemp = nodo.Nodo("Dato", [variable], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        valor = self.validarValor()
        if valor is not None:
            nodoTemp = nodo.Nodo("Dato", [valor], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        else:
            print("Error Sintaxis, Dato esperaba ser valor o variable")

    def validarLlamadaFuncion(self):
        nombre = self.validarNombre()
        if nombre is not None:
            if self.listaTokens[self.elementoActual][1] == "(":
                self.elementoActual += 1
                parametros = self.validarParametros()
                if parametros is not None:
                    if self.listaTokens[self.elementoActual][1] == ")":
                        self.elementoActual += 1
                        nodoTemp = nodo.Nodo("LlamadaFuncion", [nombre, parametros], self.listaTokens[self.elementoActual][2])
                        return nodoTemp
                    else:
                        print("Error Sintaxis, se esperaba un )")
                else:
                    if self.listaTokens[self.elementoActual][1] == ")":
                        self.elementoActual += 1
                        nodoTemp = nodo.Nodo("LlamadaFuncion", [nombre], self.listaTokens[self.elementoActual][2])
                        return nodoTemp
                    else:
                        print("Error Sintaxis, se esperaba un )")
            else:
                print("Error Sintaxis, se esperaba un (")
        else:
            print("Error Sintaxis, se esperaba un nombre")

    def asignaciones(self):
        asignacion = self.validarAsignacion()
        if asignacion is not None:
            if self.listaTokens[self.elementoActual][1] == "sal" or self.listaTokens[self.elementoActual][1] == "lizano":
                self.lista_asignaciones.append(asignacion)
                self.asignaciones()
            else:
                self.lista_asignaciones.append(asignacion)
        return self.lista_asignaciones

    def validarInstrucciones(self):
        lista = []
        self.lista_asignaciones = []
        asignaciones = self.asignaciones()
        if len(asignaciones) != 0:
            for i in asignaciones:
                lista.append(i)
        expresiones = self.validarExpresion()
        if expresiones is not None:
            lista.append(expresiones)
        llamadaFuncion = self.validarLlamadaFuncion()
        if llamadaFuncion is not None:
            lista.append(llamadaFuncion)
        nodoTemp = nodo.Nodo("Instrucciones", lista, self.listaTokens[self.elementoActual][2])
        return nodoTemp

    def validarFinal(self):
        if self.listaTokens[self.elementoActual][0] == "Final":
            self.elementoActual += 1
            dato = self.validarDato()
            if dato is not None:
                nodoTemp = nodo.Nodo("Final", [dato], self.listaTokens[self.elementoActual][2])
                return nodoTemp

    def validarValor(self):
        if self.listaTokens[self.elementoActual][0] == "Valor":
            self.elementoActual += 1
            nodoTemp = nodo.Nodo("Valor", [], self.listaTokens[self.elementoActual][2])
            return nodoTemp

    def validarParametros(self):
        parametros = self.validarParametro()
        if len(parametros) != 0:
            for i in parametros:
                self.lista_parametros.append(i)
            nodoTemp = nodo.Nodo("Parametros", self.lista_parametros, self.listaTokens[self.elementoActual][2])
            return nodoTemp

    def validarParametro(self):
        dato = self.validarDato()
        if dato is not None:
            nodoTemp = nodo.Nodo("Parametro", [dato], self.listaTokens[self.elementoActual][2])
            if self.listaTokens[self.elementoActual][1] == ",":
                self.elementoActual += 1
                self.lista_parametro.append(nodoTemp)
                self.validarParametro()
            else:
                self.lista_parametro.append(nodoTemp)
        return self.lista_parametro

    def validarExpresion(self):
        repetir = self.validarRepetir()
        if repetir is not None:
            nodoTemp = nodo.Nodo("Expresion", [repetir], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        condicional = self.validarCondicional()
        if condicional is not None:
            nodoTemp = nodo.Nodo("Expresion", [condicional], self.listaTokens[self.elementoActual][2])
            return nodoTemp

    def validarRepetir(self):
        if self.listaTokens[self.elementoActual][1] == "mezclar":
            self.elementoActual += 1
            if self.listaTokens[self.elementoActual][1] == "-":
                self.elementoActual += 1
                comparaciones = self.validarComparaciones()
                if comparaciones != None:
                    if self.listaTokens[self.elementoActual][1] == "-":
                        self.elementoActual += 1
                        if self.listaTokens[self.elementoActual][1] == "[":
                            self.elementoActual += 1
                            instrucciones = self.validarInstrucciones()
                            if instrucciones != None:
                                if self.listaTokens[self.elementoActual][1] == "]":
                                    self.elementoActual += 1
                                    nodoTemp = nodo.Nodo("Repetir", [comparaciones, instrucciones], self.listaTokens[self.elementoActual][2])
                                    return nodoTemp
                                else:
                                    print("Error Sintaxis, esperaba un ]")
                            else:
                                print("Error Sintaxis, esperaba instrucciones")
                        else:
                            print("Error Sintaxis, esperaba [")
                    else:
                        print("Error Sintaxis, esperaba - ")
            else:
                print("Error Sintaxis, esperaba -")
        else:
            print("Error Sintaxis, esperaba la palabra reservada mezclar")

    def validarCondicional(self):
        condicionalSi = self.validarCondicionalSi()
        listaCondicionalSinoSi = []
        condicionalSino = None

        while (self.listaTokens[self.elementoActual][1] == "agua_dulce"):
            listaCondicionalSinoSi.append(self.validarCondicionalSinoSi())

        if self.listaTokens[self.elementoActual][1] == "fresco":
            condicionalSino = self.validarCondicionalSino()

        if condicionalSi != None and listaCondicionalSinoSi == [] and condicionalSino == None:
            nodoTemp = nodo.Nodo("Condicional", [condicionalSi], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        elif condicionalSi != None and listaCondicionalSinoSi != [] and condicionalSino == None:
            listaCondicionalSinoSi.insert(0, condicionalSi)
            nodoTemp = nodo.Nodo("Condicional", listaCondicionalSinoSi, self.listaTokens[self.elementoActual][2])
            return nodoTemp
        elif condicionalSi != None and listaCondicionalSinoSi == [] and condicionalSino != None:
            nodoTemp = nodo.Nodo("Condicional", [condicionalSi, condicionalSino], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        elif condicionalSi != None and listaCondicionalSinoSi != [] and condicionalSino != None:
            listaCondicionalSinoSi.insert(0, condicionalSi)
            listaCondicionalSinoSi.append(condicionalSino)
            nodoTemp = nodo.Nodo("Condicional", listaCondicionalSinoSi, self.listaTokens[self.elementoActual][2])
            return nodoTemp
        else:
            print("Error! Sintaxis invalida de estructura condicional.")

    def validarAritmetica(self):
        if self.listaTokens[self.elementoActual][0] == "Aritmetica":
            self.elementoActual += 1
            nodoTemp = nodo.Nodo("Aritmetica", [], self.listaTokens[self.elementoActual][2])
            return nodoTemp

    def validarComparaciones(self):
        comparacion = self.validarComparacion()
        if comparacion != None:
            if self.listaTokens[self.elementoActual][1] == "AND" or self.listaTokens[self.elementoActual][1] == "OR":
                self.elementoActual += 1
                comparacion2 = self.validarComparacion()
                if comparacion2 != None:
                    nodoTemp = nodo.Nodo("Comparaciones", [comparacion, comparacion2], self.listaTokens[self.elementoActual][2])
                    return nodoTemp
                else:
                    print("Error Sintaxis, se esperaba estructura comparacion")
            else:
                nodoTemp = nodo.Nodo("Comparaciones", [comparacion], self.listaTokens[self.elementoActual][2])
                return nodoTemp

    def validarComparacion(self):
        dato1 = self.validarDato()
        operador = self.validarOperador()
        dato2 = self.validarDato()

        if dato1 is not None and dato2 is not None and operador is not None:
            nodoTemp = nodo.Nodo("Comparacion", [dato1, operador, dato2], self.listaTokens[self.elementoActual][2])
            return nodoTemp
        else:
            print("Error! Sintaxis incorrecta de comparacion")

    def validarCondicionalSi(self):
        if (self.listaTokens[self.elementoActual][1] == "cafe"):
            self.elementoActual += 1
            if (self.listaTokens[self.elementoActual][1] == "¿"):
                self.elementoActual += 1
                comparaciones = self.validarComparaciones()
                if comparaciones != None:
                    if (self.listaTokens[self.elementoActual][1] == "?"):
                        self.elementoActual += 1
                        if (self.listaTokens[self.elementoActual][1] == "["):
                            self.elementoActual += 1
                            instrucciones = self.validarInstrucciones()
                            if instrucciones != None:
                                final = self.validarFinal()
                                if final != None:
                                    if (self.listaTokens[self.elementoActual][1] == "]"):
                                        self.elementoActual += 1
                                        nodoTemp = nodo.Nodo("CondicionalSi", [comparaciones, instrucciones, final],
                                                             self.listaTokens[self.elementoActual][2])
                                        return nodoTemp
                                    else:
                                        print("Error Sintaxis, se esperaba ]")
                                else:
                                    print("Error Sintaxis, se esperaba ]")
                            else:
                                print("Error Sintaxis, se esperaba instrucciones")
                        else:
                            print("Error Sintaxis, se esperaba [")
                    else:
                        print("Error Sintaxis, se esperaba ?")
                else:
                    print("Error Sintaxis, se esperaba comparaciones")
            else:
                print("Error Sintaxis, se esperaba ¿")
        else:
            print("Error Sintaxis, se esperaba palabra reservada cafe")

    def validarCondicionalSinoSi(self):
        if (self.listaTokens[self.elementoActual][1] == "agua_dulce"):
            self.elementoActual += 1
            if (self.listaTokens[self.elementoActual][1] == "¿"):
                self.elementoActual += 1
                comparaciones = self.validarComparaciones()
                if comparaciones != None:
                    if (self.listaTokens[self.elementoActual][1] == "?"):
                        self.elementoActual += 1
                        if (self.listaTokens[self.elementoActual][1] == "["):
                            self.elementoActual += 1
                            instrucciones = self.validarInstrucciones()
                            if instrucciones != None:
                                final = self.validarFinal()
                                if final != None:
                                    if (self.listaTokens[self.elementoActual][1] == "]"):
                                        self.elementoActual += 1
                                        nodoTemp = nodo.Nodo("CondicionalSinoSi", [comparaciones, instrucciones, final],
                                                             self.listaTokens[self.elementoActual][2])
                                        return nodoTemp
                                    else:
                                        print("Error Sintaxis, se esperaba ]")
                            else:
                                print("Error Sintaxis, se esperaba instrucciones")
                        else:
                            print("Error Sintaxis, se esperaba[")
                    else:
                        print("Error Sintaxis, se esperaba ?")
            else:
                print("Error Sintaxis, se esperaba ¿")
        else:
            print("Error Sintaxis, palabra reservada agua_dulce")

    def validarCondicionalSino(self):
        if (self.listaTokens[self.elementoActual][1] == "fresco"):
            self.elementoActual += 1
            if (self.listaTokens[self.elementoActual][1] == "["):
                self.elementoActual += 1
                instrucciones = self.validarInstrucciones()
                if instrucciones != None:
                    final = self.validarFinal()
                    if final is not None:
                        if (self.listaTokens[self.elementoActual][1] == "]"):
                            self.elementoActual += 1
                            nodoTemp = nodo.Nodo("CondicionalSino", [instrucciones, final], self.listaTokens[self.elementoActual][2])
                            return nodoTemp
                        else:
                            print("Error Sintaxis, se esperaba ]")

    def validarNombre(self):
        if self.listaTokens[self.elementoActual][0] == "Nombre":
            self.elementoActual += 1
            nodoTemp = nodo.Nodo("Nombre", [], self.listaTokens[self.elementoActual][2])
            return nodoTemp

    def validarOperador(self):
        if self.listaTokens[self.elementoActual][0] == "Operador":
            self.elementoActual += 1
            nodoTemp = nodo.Nodo("Operador", [], self.listaTokens[self.elementoActual][2])
            return nodoTemp

    def ejecutarProfundidadPrimero(self, arbol):
        self.printNodo(arbol.nodo)
        for hijo in arbol.hijos:
            self.ejecutarProfundidadPrimero(hijo)

    def printNodo(self, nodo):
        lista_hijos = []
        for i in nodo.lista_hijos:
            lista_hijos.append(i.tipoToken)
        print("< " + str(nodo.tipoToken) + ",  " + str(lista_hijos) + ",  " + str(nodo.lineaToken) + " >")